Docker-compose Role
===================

This roles deploys a workload using docker-compose


Requirements
------------

TBD

Role Variables
--------------

> **This variables must be declared to run this role.**

### docker-compose-workload-repo

```yaml
docker-compose-workloads:
    - name: odoo
    - repo: 'https://gitlab.com/librecoop/librecoop-odoo-docker'
    - version: main 
```

Each workload vars:

- `name`: The name of the workload
- `repo`: URL of the git repo to clone
- `version`: Version string (git branch or tag).


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: librecoop.docker-compose
      docker-compose-workloads:
        - name: odoo
          repo: 'https://gitlab.com/librecoop/librecoop-odoo-docker'
          version: main 
```

License
-------

GPLv3

Author Information
------------------

librecoop https://www.librecoop.es
